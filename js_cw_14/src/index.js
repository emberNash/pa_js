import React from 'react';
import ReactDOM from 'react-dom';

const Months = () => {
	return (
		<ul>
			<li>Січень</li>
			<li>Лютий</li>
			<li>Березень</li>
			<li>Квітень</li>
			<li>Травень</li>
			<li>Червень</li>
			<li>Липень</li>
			<li>Серпень</li>
			<li>Вересень</li>
			<li>Жовтень</li>
			<li>Листопад</li>
			<li>Грудень</li>
		</ul>
	)
}

const MonthsTitle = () => {
	return <h2>Місяці року</h2>
}
const Days = () => {
	return (
		<ul>
			<li>Понеділок</li>
			<li>Вівторок</li>
			<li>Середа</li>
			<li>Четвер</li>
			<li>П'ятниця</li>
			<li>Субота</li>
			<li>Неділя</li>
		</ul>
	)
}

const DaysTitle = () => {
	return <h2>Дні тижня</h2>
}

const Zodiacs = () => {
	return (
		<ul>
			<li>Овен</li>
			<li>Телець</li>
			<li>Близнята</li>
			<li>Рак</li>
			<li>Лев</li>
			<li>Діва</li>
			<li>Терези</li>
			<li>Скорпіон</li>
			<li>Стрілець</li>
			<li>Козоріг</li>
			<li>Водолій</li>
			<li>Риби</li>
		</ul>
	)
}

const ZodiacsTitle = () => {
	return <h2>Знаки зодіаку</h2>
}


const Result = () => {
	return (
		<div>
			<MonthsTitle></MonthsTitle>
			<Months></Months>
			<DaysTitle></DaysTitle>
			<Days></Days>
			<ZodiacsTitle></ZodiacsTitle>
			<Zodiacs></Zodiacs>
		</div>
	)
}

ReactDOM.render(<Result></Result>, document.querySelector('#root'));