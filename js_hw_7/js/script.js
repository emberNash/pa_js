//---------------------------------------------------------------------------
function createNewUser() {
	this.firstName = prompt('vvedite imya', 'Pavlo');
	this.lastName = prompt('vvedite familiy', 'Ryschenko');
	this.birthday = prompt('vvedite datu rozhdeniya v formate dd.mm.yyyy', '10.09.1992');
}
// ----------------------------------------------
createNewUser.prototype.getLogin = function () {
	let login = this.firstName[0] + this.lastName;
	return `login: ${login.toLowerCase()}`;
}
// ----------------------------------------------	
createNewUser.prototype.getpassword = function () {
	let password = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
	return `password: ${password}`;
}
// ----------------------------------------------	
createNewUser.prototype.getAge = function () {
	let today = new Date();
	let year = today.getFullYear();
	let age = year - parseInt(this.birthday.slice(-4));
	return `vozrast: ${age}`;
}
// ----------------------------------------------
const newUser = new createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getpassword());
console.log(newUser.getAge());
// Viselica na ES6---------------------------------------------------------------------------
var words = [
	"программа",
	"макака",
	"прекрасный",
	"оладушек"
];
// Выбираем случайное слово
const word = words[Math.floor(Math.random() * words.length)];
// Создаем итоговый массив
const answerArray = [];
for (let i = 0; i < word.length; i++) {
	answerArray[i] = "_";
}
let remainingLetters = word.length;
// Игровой цикл
while (remainingLetters > 0) {
	// Показываем состояние игры
	alert(answerArray.join(" "));
	// Запрашиваем вариант ответа
	let guess = prompt("Угадайте букву, или нажмите Отмена для выхода из игры.");
	if (guess === null) {
		// Выходим из игрового цикла
		break;
	} else if (guess.length !== 1) {
		alert("Пожалуйста, введите одиночную букву.");
	} else {
		// Обновляем состояние игры
		for (let j = 0; j < word.length; j++) {
			if (word[j] === guess) {
				answerArray[j] = guess;
				remainingLetters--;
			}
		}
	}
	// Конец игрового цикла
}
// Отображаем ответ и поздравляем игрока
alert(answerArray.join(" "));
alert(`Отлично! Было загадано слово ${word}`);