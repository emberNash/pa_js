window.onload = () => {
	const btn = document.querySelector(".btn");
	btn.onclick = function () {
		let dia = document.createElement('input');//sozdaem input
		btn.after(dia);
		let draw = document.createElement('button'); //sozdaem knopku
		draw.textContent = 'draw!'
		dia.after(draw);
		draw.onclick = function () {
			//proveerkana NaN-------------------------------
			let nan = parseFloat(dia.value);
			if (isNaN(nan) === true) {
				alert(`vvedite chislo!`);
			} else {
				for (j = 0; j < 10; j++) {
					//cykl dlya stolbcov
					let row = document.createElement('div');
					row.style.display = `flex`;
					draw.after(row);
					// cykl dlya strok
					for (let index = 0; index < 10; index++) {
						let d = document.createElement('div');
						d.classList.add('sircle');
						d.style.borderRadius = `50%`;
						d.style.borderRadius = `50%`;
						d.style.margin = `5px`;
						d.style.boxSizing = `border-box`;
						d.style.height = `${parseFloat(dia.value)}px`;
						d.style.width = `${parseFloat(dia.value)}px`;
						d.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
						row.append(d);
					}
				}
				//udalenie po kliku
				let [...targets] = document.querySelectorAll(".sircle");
				targets.forEach(target => {
					target.onclick = () => {
						target.remove();
					}
				})//konec risovaniya kruzhkov
			}//konec  f-ii onclick na knopke draw
		}//konec f-ii onclick na knopke dimeter
	};
}