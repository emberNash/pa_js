// Створи клас, який буде створювати користувачів з ім'ям та прізвищем. Додати до класу метод для виведення імені та прізвища
let nameInput = document.querySelector('.name');
let snameInput = document.querySelector('.sname');
let write = document.querySelector('.write');
let read = document.querySelector('.read');
let show = document.querySelector('.show-user');
let user1 = [];

class User {
	constructor(name, sname) {
		this.name = name;
		this.sname = sname;
	}
};

write.addEventListener('click', () => {
	user1 = new User(nameInput.value, snameInput.value);
	nameInput.value = '';
	snameInput.value = '';
});

read.addEventListener('click', () => {
	show.textContent = `name: ${user1.name} surname: ${user1.sname}`;
});

// Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний

let liN2 = document.querySelector('.task2>ul>li').nextElementSibling;
liN2.previousElementSibling.style.backgroundColor = 'blue';
liN2.nextElementSibling.style.backgroundColor = 'red';

// Створи див висотою 400 пікселів і додай на нього подію наведення мишки. При наведенні мишки виведіть текст координати, де знаходиться курсор мишки

let task3 = document.querySelector('.task3');
let task3Out = document.querySelector('.task3>span');
task3.addEventListener('mousemove', (e) => {
	task3Out.textContent = `x: ${e.clientX} y: ${e.clientY}`;
});

// Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута

let [...buttons] = document.querySelectorAll('.task4> button');
let pushed = document.querySelector('.pushed');

buttons.forEach(e => {
	e.addEventListener('click', () => {
		buttons.forEach(e => {
			e.classList.remove('_push');
		})
		e.classList.add('_push');
		pushed.textContent = `pushed ${e.textContent}`;
	})
});


// Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці

let innerWidth = window.innerWidth;
let innerHeight = window.innerHeight;
let warp = document.querySelector('.warp');

warp.addEventListener('mouseover', () => {
	warp.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
	warp.style.top = `${Math.floor(Math.random() * innerHeight)}px`;
	warp.style.left = `${Math.floor(Math.random() * innerWidth)}px`;
})

// Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body

let fillColor = document.querySelector('.fill-color');
let fill = document.querySelector('.fill');

fill.addEventListener('click', () => {
	document.body.style.background = fillColor.value;
});

// Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль

let login = document.querySelector('.login');
login.addEventListener('input', () => {
	console.log(login.value);
})

// Створіть поле для введення даних у полі введення даних виведіть текст під полем

let area = document.querySelector('.textarea');
let text = document.querySelector('.text');
area.addEventListener('input', () => {
	text.textContent = `${area.value}`;
})

