//pizza size
let [...sizeVars] = document.querySelectorAll('.radioIn');

class Order {
	constructor(name, number, email, size, sauses, toppings, total) {
		this.name = name;
		this.number = number;
		this.email = email;
		this.size = size;
		this.sauses = sauses;
		this.toppings = toppings;
		this.total = total;
	}
}

let priceList = {
	small: 100,
	mid: 140,
	big: 190,
}
// -------------------
let x1 = 0, x2 = 0, x3 = 0;
let price = (x1, x2, x3) => { return x1 + x2 + x3 }
// -------------------

let total = document.querySelector('.price');
let sauce = document.querySelector('.sauces');
let toping = document.querySelector('.topings');

let priseSpan = document.createElement('span');
priseSpan.classList.add('pspan');
priseSpan.innerText = `190 грн`;


total.append(priseSpan);

let saucesSpan = document.createElement('span');
saucesSpan.classList.add('pspan');
saucesSpan.innerText = `0 грн`;
sauce.append(saucesSpan);

let topingsSpan = document.createElement('span');
topingsSpan.classList.add('pspan');
topingsSpan.innerText = `0 грн`;
toping.append(topingsSpan);

// -zminni dlya zberezhennya dannuh zamovlennya----------
let pizzaSize = '', saucesAmount = [], toppingsAmount = [];
// -----------------

sizeVars.forEach((e) => {
	e.addEventListener('click', () => {
		for (let key in priceList) {
			if (key == e.value) {
				x1 = parseInt(priceList[key]);
				priseSpan.innerText = `${price(x1, x2, x3)} грн`;
				pizzaSize = key;
			}
		}
	})
})
//drag&drop
const zone = document.querySelector('.table');

allowDrop = event => event.preventDefault();

zone.ondragover = allowDrop;

const [...ingridients] = document.querySelectorAll('.ingridients img');
const sauces = {
	sauceClassic: 15,
	sauceBBQ: 20,
	sauceRikotta: 30
};
const topings = {
	moc1: 20,
	moc2: 30,
	moc3: 35,
	telya: 50,
	vetch1: 20,
	vetch2: 40
};

ingridients.forEach((e) => {
	e.addEventListener("dragstart", (event) => {
		event.dataTransfer.setData('id', event.target.id);
		event.dataTransfer.setData('text', event.target.nextElementSibling.innerText);
	});
});

drop = (event) => {
	let itemId = event.dataTransfer.getData('id');
	let itemtext = event.dataTransfer.getData('text');
	let copy = document.getElementById(itemId).cloneNode();
	event.target.after(copy);
	for (let key in sauces) {
		if (key == itemId) {
			x2 += sauces[key];
			priseSpan.innerText = `${price(x1, x2, x3)} грн`;
			let sum2 = parseInt(saucesSpan.innerText) + sauces[key];
			saucesSpan.innerText = ` ${sum2} грн`;

			saucesAmount.push(itemtext);
		}
	}

	for (let key in topings) {
		if (key == itemId) {
			x3 += topings[key];
			priseSpan.innerText = `${price(x1, x2, x3)} грн`;
			let sum2 = parseInt(topingsSpan.innerText) + topings[key];
			topingsSpan.innerText = ` ${sum2} грн`;
			toppingsAmount.push(itemtext);
		}
	}

}

zone.ondrop = drop;

// проверка формі та збереження

let [...allInputs] = document.querySelectorAll(".grid > input");

let inputsRez = allInputs.map(function (element) {
	return element;
}).filter((element) => {
	return element.type != "button" && element.type != "reset";
})

const validate = (target) => {
	switch (target.id) {
		case "name":
			return /^[A-zА-я_ ]{2,}$/i.test(target.value);
		case "phone":
			return /^\+380\d{9}$/.test(target.value);
		case "email":
			return /^[a-z.\d]+@[a-z._]+\.[a-z._]{1,4}$/i.test(target.value);
		default:
			throw new Error("Невірний виклик регулярного виразу");
	}
}
// -----------------------
let saveButton = document.querySelector(".grid > [type=button]");
let clearButton = document.querySelector(".grid > [type=reset]");
// -----------------------

clearButton.addEventListener("click", () => {
	inputsRez.forEach((e) => { e.value = '' });
});
// -----------------------
saveButton.addEventListener("click", () => {
	//Функція валідації після натиску кнопки
	let validateRez = inputsRez.map(function (element) {
		return validate(element);
	});
	//Перевіряємо чи немає помилок в інпутах
	if (!validateRez.includes(false)) {
		//Якщо нема помилок, записуємо в localStorage
		let order = new Order(inputsRez[0].value, inputsRez[1].value, inputsRez[2].value, pizzaSize, saucesAmount, toppingsAmount, parseInt(priseSpan.innerText));

		localStorage.setItem('order', JSON.stringify(order));
		alert("Заповнені дані виведені у localStorage");
		document.location = './thank-you.html';
	}
	else { alert("Перевірте дані замволення"); }

});

//discount
let clientWidth = document.documentElement.clientWidth;
let clientHeight = document.documentElement.clientHeight;
let banner = document.getElementById('banner');

banner.addEventListener('mouseover', () => {
	banner.style.bottom = `${Math.floor(Math.random() * clientHeight)}px`;
	banner.style.right = `${Math.floor(Math.random() * clientWidth)}px`;
})