//==================classwork Calculator ==============================================
function Calculator(x, y, z) {
	this.read = function () {
		this.num1 = parseFloat(prompt(`vvedite chislo`, `2`));
		this.num2 = parseFloat(prompt(`vvedite vtoroe chislo`, `3`));
	}
	// ==============================================
	this.sum = function () {
		return this.num1 + this.num2;
	};
	// ==============================================
	this.mul = function () {
		return this.num1 * this.num2;
	};
}
// ==============================================
let a = new Calculator();
a.read();
a.sum();
a.mul();
console.log(`${a.num1} + ${a.num2} = ${a.sum()}`);
console.log(` ${a.num1} x ${a.num2} = ${a.mul()}`);

//==================homework #1 ==============================================
let data = [
	{ name: 'Ivan', age: 60 },
	{ name: 'Vasya', age: 15 },
	{ name: 'Petro', age: 23 },
	{ name: 'Karina', age: 21 },
	{ name: 'Evhen', age: 45 },
	{ name: 'Nadiya', age: 30 },
	{ name: 'Gleb', age: 24 },
];

let type = prompt(`ukazhite sortirovku: up ili down`, `up`);

function Human(array, type) {
	// this.array = array; // якщо використаємо такий запис - метод фільтр відфільтрує масив data
	this.array = [...array]; 	// тому ми використовуємо копію цього масиву 

	if (type == 'up') {
		this.array.sort(this.sortByAge);
	} else if (type == 'down') {
		this.array.sort(this.sortByAgeDown);
	} else {
		alert(`ukazhite sortirovku`);
	}

}

Human.prototype.sortByAge = function (a, b) {
	if (a.age > b.age) {
		return 1;
	} else {
		return -1;
	}
}
Human.prototype.sortByAgeDown = function (a, b) {
	if (a.age < b.age) {
		return 1;
	} else {
		return -1;
	}
}

let hum1 = new Human(data, type);
console.log(hum1);

//==================homework #2 ====================================================================================

//======одна з головних завдань конструктора - приймати дані та створювати нові екзмпляри на їх основі==============
function Product(name, category, country, price, amount) {
	this.name = name;
	this.category = category;
	this.country = country;
	this.price = price;
	this.amount = amount;
};
//======переважна більшість методів, які проводять специфічні операції з певними властивостями зберігаються=========
//======на рівні екземпляру=========================================================================================
Product.prototype.sum = function () {
	const sum = this.price * this.amount;
	return console.log(`${this.name} total sum: ${this.price}$ x ${this.amount} = ${sum}$`);
	;
};
//===================================================================================================================
let playStation = new Product(`PlayStation 5`, `console`, `Japan`, 499, 20);
playStation.sum();
