let [...buttons] = document.querySelectorAll('.btn');
let press = false;

buttons.forEach(element => {
	document.addEventListener('keydown', (e) => {
		if (e.key.toUpperCase() === element.textContent.toUpperCase()) {
			if (!press) {
				element.classList.add('_pressed');
				press = true;
			}
		}
	});
	document.addEventListener('keyup', (e) => {
		if (e.key.toUpperCase() === element.textContent.toUpperCase()) {
			if (press) {
				element.classList.remove('_pressed');
				press = false;
			}
		}
	});
});





