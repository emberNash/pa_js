// --------------------STOPWATCH--------------------------------
let stopWatchScreen = document.querySelector('.stopwatch__screen');
// ----------------------------------------------------
let min = document.querySelector('.minuts');
let sec = document.querySelector('.seconds');
let mili = document.querySelector('.milisec');
// ----------------------------------------------------
let start = document.querySelector('.start');
let pause = document.querySelector('.stop');
let clear = document.querySelector('.clear');
// ----------------------------------------------------
let counter0 = 00;
let counter1 = 00;
let counter2 = 00;
// ----------------------------------------------------
const miliCount = () => {
	counter0 += 10;
	mili.textContent = counter0;
	if (counter0 === 1000) {
		counter1++;
		counter0 = 00;
		sec.textContent = counter1;
	} else if (counter1 === 60) {
		counter2++;
		counter1 = 00;
		min.textContent = counter2;
	}
}
// ----------------------------------------------------
start.onclick = () => {
	// stopWatchScreen.style.backgroundColor = `#3cb371`;
	stopWatchScreen.classList.remove('_green', '_red', '_grey');
	stopWatchScreen.classList.add('_green');
	// ----------------

	timer = setInterval(miliCount, 10);
}
// ----------------------------------------------------
pause.onclick = () => {
	// stopWatchScreen.style.backgroundColor = `#FF4500`;
	stopWatchScreen.classList.remove('_green', '_red', '_grey');
	stopWatchScreen.classList.add('_red');
	clearInterval(timer);
}
clear.onclick = () => {
	// stopWatchScreen.style.backgroundColor = `#C0C0C0`;
	stopWatchScreen.classList.remove('_green', '_red', '_grey');
	stopWatchScreen.classList.add('_grey');
	clearInterval(timer);
	counter0 = 00;
	counter1 = 00;
	counter2 = 00;
	mili.textContent = counter0;
	sec.textContent = counter1;
	min.textContent = counter2;
}
// --------------------PHONE-NUMBER-CHECK--------------------------------

let part2 = document.querySelector('.part-2');
let input = document.createElement('input');
input.setAttribute(`placeholder`, `000-00-00-00`);
let save = document.createElement('button');
const check = /^\d{3}-\d{3}-\d{2}-\d{2}$/;
save.textContent = `save`;
part2.append(input);
input.after(save);
save.onclick = () => {
	let phone = input.value;
	let res = phone.match(check);
	if (Array.isArray(res)) {
		input.classList.add('_green');
		let sucsess = () => {
			document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg'
		}
		setTimeout(sucsess, 1000)
	} else {
		let error = document.createElement('div');
		error.textContent = `error`;
		part2.prepend(error);
		let del = () => {
			error.remove();
		}
		setTimeout(del, 2000)
	}
}
// -------------------------SLIDER-------------------------------------
let [...planets] = document.querySelectorAll('.planet');
let run = document.querySelector('.run');
let stp = document.querySelector('.break');
let count = 0;

const swipe = () => {
	planets.forEach(e => {
		if (e.classList.contains('_active')) {
			e.classList.remove('_active')
		}
	})
	planets[count].classList.add('_active');
	count++;
	if (count === planets.length) {
		count = 0;
	}
	console.log(count);

}
// ---------------------------------------------------------------------
run.onclick = () => {
	slider = setInterval(swipe, 3000);
}
stp.onclick = () => {
	clearInterval(slider);
}